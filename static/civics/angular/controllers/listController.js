angular.module('civics.list_controller', [])

/**
 *   Controller for list displays
 */
.controller("ListController", [
  'items',
  'Categories',
  'meta',
  '$scope',
  '$rootScope',
  '$http',
  'Downloader',
  function(
      items,
      Categories,
      meta,
      $scope,
      $rootScope,
      $http,
      Downloader
  )
{

    /**
     *  Content setup
     */
    this.markers = items;
    for(i in this.markers)
      this.markers[i].filtered = false;  // Show markers by default

    /**
     *  Section state
     */
    this.showing_map = true;
    this.count = this.markers.length;
    this.format  = 'list';

    // Close responsive menu popup if open
    document.querySelector('.region-toolbar__right').classList.remove('active')

    /**
     *  Pager setup and methods
     */
    this.starting_index = 0;
    this.items_per_page = 48;
    this.starting_index = 0;
    this.ending_index   = this.items_per_page;
    this.current_items  = 0;

    // Get previous results
    this.previousPage = function(){
        var next_index = this.starting_index - this.items_per_page;
        this.starting_index = next_index < 0 ? 0 : next_index;
        this.set_current_items();
    }
    // Get next results
    this.nextPage = function(){
        var next_index = this.starting_index + this.items_per_page;
        this.starting_index = next_index > this.count ? this.starting_index : next_index;
        this.set_current_items();
    }
    // Get number of current displayed elements
    this.set_current_items = function(){
        var factor = parseFloat(this.count - this.starting_index)/parseFloat(this.items_per_page);
        this.current_items = factor > 1 ? this.items_per_page : this.count - this.starting_index;
    }
    this.set_current_items();

    /**
     *  Setup categories and filter settings
     */


    this.cities       = Categories.cities;
    this.categories   = Categories.categories;
    this.perspectives = Categories.perspectives;
    this.agents       = Categories.agents;
    this.activities   = Categories.activities;

    categories = [
        'cities',
        'categories',
        'perspectives',
        'agents'
    ];


    // Selected categories
    this.selected_categories = {};
    for(i in categories)
        this.selected_categories[ categories[i] ] = []

    // Selected category tabs
    this.selected_tabs = [];

    // Active elements in legend
    this.active_legend_items = {};

    // Auxiliar function to calculate intersection between arrays
    function intersect(a, b) {
        var t;
        if (b.length > a.length) t = b, b = a, a = t; // indexOf to loop over shorter
        return a.filter(function (e) {
            return b.indexOf(e) > -1;
        });
    }

    /**
     *  Reset categories to default inactive state
     */
    this.resetLegend = function(){
        for(var i in categories){
            this.active_legend_items[categories[i]] = {};
            for(var s in this[categories[i]]){
                this.active_legend_items[categories[i]][s] = false;
            }
        }
    }

    // Apply default state
    this.resetLegend();

    this.filterMarkers = function()
    {
        var c = meta.count;
        var filters_length = this.selected_tabs.length;

        if(filters_length > 0){
            items.forEach( angular.bind(this, function(marker){
                // Filter by category
                for(var cat in this.selected_categories){
                    var selected_categories = this.selected_categories[cat];
                    if(selected_categories.length > 0){
                        // Every marker is visible by default in each category
                        marker.filtered = false;
                        // If marker category is not in active list filter it
                        var marker_categories = marker[cat];
                        if(Array.isArray(marker_categories)){
                            marker.filtered = intersect(marker_categories, selected_categories).length == 0;
                        } else {
                            marker.filtered = selected_categories.indexOf(marker_categories) == -1;
                        }
                        if(marker.filtered){
                            c--;
                            break;
                        }
                    }
                }
            }));
        } else {
            items.forEach( angular.bind(this, function(marker){
                marker.filtered = false;
            }));
            for(var cat in this.selected_categories){
                this.selected_categories[cat] = [];
            }
        }
        this.count = c;
    };

    /**
     *  Toggle a filter
     */
    this.toggleFilter = function(category, subcategory, city){
        var i = this.selected_categories[category].indexOf(subcategory);
        this.active_legend_items[category][subcategory] = !this.active_legend_items[category][subcategory];
        if(i == -1){
            if(city) {
                this.selected_categories[category].push(parseInt(subcategory));
                this.selected_tabs.push({ 'k' : category, 'v': subcategory, 'n' : city.name });
            } else {
                this.selected_categories[category].push(subcategory);
                this.selected_tabs.push({ 'k' : category, 'v': subcategory, 'n' : Categories[category][subcategory] });
            }
        } else {
            var i = this.selected_categories[category].indexOf(subcategory);
            this.selected_categories[category].splice(i, 1);
        }
        this.filterMarkers();
        this.show_help = false;
    }

    /**
     *  Remove a filter
     */
    this.removeFilter = function(index){
        var f = this.selected_tabs.splice(index, 1);
        var i = this.selected_categories[f[0].k].indexOf(f[0].v);
        this.selected_categories[f[0].k].splice(i, 1);
        this.filterMarkers();
        this.show_help = false;
    }

    /**
     *  Remove all filters
     */
    this.removeFilters = function(){
        this.selected_tabs = [];
        this.resetLegend();
        this.filterMarkers();
        this.show_help = false;
    }

    /**
     *  Show popup of selected markers
     */
    this.show = function(marker){
        var url = '/api/active?id=';
        $http.get(url + marker.pk, {
            ignoreLoadingBar: true,
        }).then( angular.bind(this, function(response){
            $rootScope.$broadcast('open-marker', response.data);
        }));
    }

    this.cityname = function(id){
        return Categories.cities[id];
    }

    /**
     *   Download XLS with filtered initiatives
     */
    this.download_xls = function(){
        this.show_help = false;
        Downloader.get(this.section, this.selected_categories);
    }

    $scope.$on('open-marker', angular.bind(this, function(){
        this.sharing_url = false;
        this.show_help = false;
    }));
}]);
