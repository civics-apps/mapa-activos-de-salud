angular.module('civics.map_controller', [])

.controller("MapController", [
    '$scope',
    'Settings',
    'Actives',
    'Categories',
    'leafletData',
    'items',
    'meta',
    'Downloader',
    '$route',
    '$location',
    function(
        $scope,
        Settings,
        Actives,
        Categories,
        leafletData,
        items,
        meta,
        Downloader,
        $route,
        $location
){
    /**
     *  Map setup
     */

    // Check url params
    var params = $location.search();

    // Inject resolved data into the map
    var _map = {};
    //  a feature group to hold related initiative connections
    this.relations = new L.FeatureGroup();
    leafletData.getMap("civics-map").then(angular.bind(this, function(map)
    {
        _map = map;
        for(city in items){
            map.addLayer( items[city] );
        }
        this.relations.addTo(map);
    }));

    // Map defaults
    this.defaults = Settings.map_defaults.defaults;
    this.controls = Settings.map_controls;
    this.layers   = Settings.map_layers;
    if(params.center){
        var coords = params.center.split(",");
        this.center = { lat: parseFloat(coords[0]), lng: parseFloat(coords[1]), zoom: parseInt(coords[2]) };
    } else {
        this.center = Settings.map_defaults.center;
    }
    this.highlight_markers = false;

    /**
     *  Section state
     */
    this.count    = meta.count;
    this.format   = 'map';

    // A state to reflect if clusters are fully enabled in current view
    // Map declusters when it reaches the given zoom threshold
    // true : { pruneCluster layer }.Cluster.Size = maximumClusterSize
    // false: { pruneCluster layer }.Cluster.Zoom = minimumClusterSize
    // @see https://github.com/SINTEF-9012/PruneCluster/issues/52#issuecomment-102491577
    var minimumClusterSize = .001;
    var maximumClusterSize = 120;
    var clustersEnabled    = true;
    var zoomThreshold      = 9;

    // Close responsive menu popup if open
    document.querySelector('.region-toolbar__right').classList.remove('active')

    /**
     *  Setup categories and filter settings
     */
    this.cities       = Categories.cities;
    this.categories   = Categories.categories;
    this.perspectives = Categories.perspectives;
    this.agents       = Categories.agents;

    // Init filters
    categories = [
        'cities',
        'categories',
        'perspectives',
        'agents'
    ];

    // Selected categories
    this.selected_categories = {};
    for(i in categories)
        this.selected_categories[ categories[i] ] = []

    // Selected category tabs
    this.selected_tabs = [];
    for(var i in categories){
        var category = categories[i];
        if(params[category]){
            var subcategories = params[category].split(",");
            if(category=='cities') {
                for(s in subcategories)
                    this.selected_categories['cities'].push( parseInt(subcategories[s]) );
            } else {
                this.selected_categories[ category ] = subcategories;
            }
            for(var j in subcategories){
                if(subcategories[j] !== '') {
                    this.selected_tabs.push({
                        'k' : category,
                        'v' : subcategories[j],
                        'n' : Categories[category][subcategories[j]]
                    });
                }
            }
        }
    }

    // Active elements in legend
    this.active_legend_items = {};

    // Popup states
    this.sharing_url = false;
    this.download_modal_visible = false;
    this.show_help = false;

    /**
     *  Reset categories to default inactive state
     */
    this.resetLegend = function(){
        for(var i in categories){
            this.active_legend_items[categories[i]] = {};
            for(var s in this[categories[i]]){
                this.active_legend_items[categories[i]][s] = false;
            }
        }
    }

    // Apply default state
    this.resetLegend();

    // Auxiliar function to calculate intersection between arrays
    function intersect(a, b) {
        var t;
        if (b.length > a.length) t = b, b = a, a = t; // indexOf to loop over shorter
        return a.filter(function (e) {
            return b.indexOf(e) > -1;
        });
    }

    /**
     *  Filter markers by active categories
     */
    this.filterMarkers = function()
    {
        var c = meta.count;
        var filters_length = this.selected_tabs.length;
        for(city in items){
            var markers = items[city].Cluster._markers;
            if(filters_length > 0){
                markers.forEach( angular.bind(this, function(marker){
                    for(var cat in this.selected_categories){
                        var selected_categories = this.selected_categories[cat];
                        if(selected_categories.length > 0){
                            // Every marker is visible by default in each category
                            marker.filtered = false;
                            // If marker category is not in active list filter it
                            var marker_categories = marker.data[cat];
                            if(Array.isArray(marker_categories))
                            {
                                marker.filtered = intersect(marker_categories, selected_categories).length == 0;
                            } else {
                                marker.filtered = selected_categories.indexOf(marker_categories) == -1;
                            }
                            if(marker.filtered){
                                c--;
                                break;
                            }
                        }
                    }
                }));
            } else {
                markers.forEach( angular.bind(this, function(marker){
                    marker.filtered = false;
                }));
                for(var cat in this.selected_categories){
                    this.selected_categories[cat] = [];
                }
            }
            this.count = c;
            items[city].ProcessView();
        }
    };

    this.filterMarkers();

    /**
     *  Toggle a filter
     */
    this.toggleFilter = function(category, subcategory, city){
        // Disable popups if visible
        this.sharing_url = false;
        this.show_help = false;

        var i = this.selected_categories[category].indexOf(subcategory);

        this.active_legend_items[category][subcategory] = !this.active_legend_items[category][subcategory];
        if(i == -1){
            if(city) {
                this.selected_categories[category].push(parseInt(subcategory));
                this.selected_tabs.push({ 'k' : category, 'v': subcategory, 'n' : city.name });
                _map.setView(city.coords, 10);
            } else {
                this.selected_categories[category].push(subcategory);
                this.selected_tabs.push({ 'k' : category, 'v': subcategory, 'n' : Categories[category][subcategory] });
            }
        } else {
            var i = this.selected_categories[category].indexOf(subcategory);
            this.selected_categories[category].splice(i, 1);
        }
        this.filterMarkers();
        this.show_help = false;
    }

    /**
     *  Remove a filter
     */
    this.removeFilter = function(index){
        var f = this.selected_tabs.splice(index, 1);
        var i = this.selected_categories[f[0].k].indexOf(f[0].v);
        this.selected_categories[f[0].k].splice(i, 1);
        this.active_legend_items[f[0].k][f[0].v] = false;
        this.filterMarkers();
        this.sharing_url = false;
        this.show_help = false;
    }

    /**
     *  Remove all filters
     */
    this.removeFilters = function(){
        this.selected_tabs = [];
        this.resetLegend();
        this.filterMarkers();
        this.sharing_url = false;
        this.show_help = false;
    }

    /**
     *   Download filtered actives
     */
    this.download = function(format){
        Downloader.get(format, this.section, this.selected_categories);
        this.sharing_url = false;
        this.show_help = false;
        this.download_modal_visible = false;
    }


    /**
     *   Reset map view to initial state
     */
    this.expand = function(){
        _map.setView([0, -26], 3);
        this.sharing_url = false;
        this.show_help = false;
    }

    this.shareUrl = function(){
        this.sharing_url = !this.sharing_url;
        this.download_modal_visible = false;
        this.show_help = false;
        if(this.sharing_url) {
            var base_url = $location.absUrl().split("?")[0];
            this.shared_url = base_url + "?center=" + this.center.lat.toFixed(4) + "," + this.center.lng.toFixed(4) + "," + this.center.zoom;
            for(var category in this.selected_categories){
                var subcategories = this.selected_categories[category];
                if( subcategories.length > 0){
                    this.shared_url += "&" + category + "=";
                    for(var j in subcategories) {
                        this.shared_url += subcategories[j] + ",";
                    }
                }
            }
        }
    }

    this.showDownloadModal = function(){
        this.download_modal_visible = !this.download_modal_visible;
        this.sharing_url = false;
        this.show_help = false;
    }

    this.showHelpModal = function(){
        this.show_help = !this.show_help;
        this.download_modal_visible = false;
        this.sharing_url = false;
    }

    var featured_nodes = document.querySelectorAll('.featured');
    var related_nodes  = document.querySelectorAll('.related');

    $scope.$on('open-marker', angular.bind(this, function(event, args){
        this.sharing_url = false;
        this.show_help = false;
        // Highlight selected initiative and related actives
        // Fade all markers
        this.highlight_markers = true;
        // If there's any selected marker unselect it and its related actives
        if(featured_nodes.length>0){
            featured_nodes.forEach(function(marker){
                marker.classList.remove('featured')
            });
        }
        if(related_nodes.length>0){
            related_nodes.forEach( function(marker){
                marker.classList.remove('related');
            });
        };
        // Highlight current marker
        var marker = document.querySelector('.cm--' + args.id);
        marker.classList.add('featured');
        var relations_network = Actives.getRelations(args.id);
        relations_network.forEach(function(i){
            var id = i[2];
            marker = document.querySelector('.cm--' + id);
            marker.classList.add('related');

        });
        featured_nodes = document.querySelectorAll('.featured');
        related_nodes  = document.querySelectorAll('.related');
        // Draw lines
        leafletData.getMap("civics-map").then(angular.bind(this, function(map)
        {
            this.relations.clearLayers();
            relations_network.forEach(function(i)
            {
              var line = new L.Polyline([
                  i[0],
                  i[1],
              ], {
                  dashArray : [15, 5],
                  color: '#111111',
                  weight: i[2] == args.id ? 3.5 : 1.25,
                  opacity: .75,
              });
              line.addTo(this.relations);
            }, this);
        }));
    }));

    // Unselect markers when closing popup
    $scope.$on('close-marker', angular.bind(this, function(){
          this.highlight_markers = false;
          this.relations.clearLayers();
    }));

    $scope.$on("leafletDirectiveMap.civics-map.moveend", function(event, args) {
        if(args.model.center.zoom >= zoomThreshold && clustersEnabled){
            console.log("Disabling clusters");
            for(var city in items){
               items[city].Cluster.Size   = minimumClusterSize;
               items[city].ProcessView();
            }
            clustersEnabled = false;
        } else if(args.model.center.zoom < zoomThreshold && !clustersEnabled){
            console.log("Disabling clusters");
            for(var city in items){
                items[city].Cluster.Size = maximumClusterSize;
                items[city].ProcessView();
            }
            clustersEnabled = true;
        }
    });
}]);
