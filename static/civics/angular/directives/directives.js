/**
 *  Get this script path to use relative paths for templates and avoid breaking
 *  the site because of the changes i18n introduces in paths (language prefixes)
 *  @see https://stackoverflow.com/questions/21103724/angular-directive-templateurl-relative-to-js-file
 */
var scripts = document.getElementsByTagName("script")
var path    = scripts[scripts.length-1].src;  //last script is the one being evaluated

angular.module('civics.directives', [])

/**
 *  Social widget block
 */
.directive('social', function(){
    return {
      restrict: 'A',
      templateUrl: path.replace('directives.js', 'social_widget.html')
      //templateUrl: 'static/civics/angular/directives/social_widget.html'
    }
})

/**
 *  Marker information in the maps
 */
.directive('markerInfo', function(){
    return {
        restrict: 'A',
        controller: 'MarkerinfoController',
        controllerAs: 'info',
        templateUrl: path.replace('directives.js', 'marker-info.html'),
        replace: true
    }
})

.controller('MarkerinfoController', function(Categories, $scope, meta, $sce, $rootScope, $http, Langs){
     /** Root scope event fired from the services that create the markers **/
     $scope.$on('open-marker', angular.bind(this, function(event, args){
          this.expanded = true;
          this.marker = args;
          this.media_src = $sce.trustAsResourceUrl(this.marker.vid);
          this.marker.category_name = Categories.categories[ this.marker.categories ];
          var active_perspectives = [ this.marker.perspective, ...this.marker.perspective_alt ];
          this.marker.perspective_names = active_perspectives.map(function(i){
              return Categories.perspectives[i];
          });
          this.marker.agent_names = this.marker.agents.map(function(i){
              return Categories.agents[i];
          });
          this.showing = meta.showing;
     }));
     this.closePopup = function(){
          this.expanded = false;
          $rootScope.$broadcast('close-marker');
     }
     this.filename = function(filepath){
         var comps = filepath.split("/");
         return comps[comps.length-1];
     }
     this.openRelatedInfo = function(id){
         $http.get('/api/active?id=' + id, {
             ignoreLoadingBar: true,
         }).then( function(response){
             $rootScope.$broadcast('open-marker', response.data);
         });
     }
     this.lang = Langs.lang;
})

/**
 *  Marker information in the maps
 */
.directive('mapActions', function(){
    return {
        restrict: 'A',
        replace: true,
        templateUrl: path.replace('directives.js', 'map-actions.html'),
        //templateUrl: 'static/civics/angular/directives/map-actions.html'
    }
})

/**
 *  Marker information in the maps
 */
.directive('leafletExpand', function(){
    return {
        restrict: 'A',
        replace: true,
        template: '<div class="leaflet-expand" ng-click="content.expand()"><span class="icon-expand"></span></div>',
    }
})

/**
 *  Marker information in the maps
 */
.directive('mapFilters', function(){
    return {
        restrict: 'A',
        replace: true,
        templateUrl: path.replace('directives.js', 'map-filters.html'),
        //templateUrl: 'static/civics/angular/directives/map-filters.html'
    }
})

/**
 *  Marker information in the maps
 */
.directive('search', function(){
    return {
        restrict: 'A',
        replace: true,
        controller: 'SearchController',
        controllerAs: 'search',
        templateUrl: path.replace('directives.js', 'search.html'),
        //templateUrl: 'static/civics/angular/directives/search.html'
    }
})

.controller('SearchController', function($scope, $http, $rootScope, leafletData){
     this.results = [];
     this.type = 'name';

     this.query = function(){
        if(this.name.length > 2){
            $http.get('/api/autocomplete?t=' + this.type + '&q=' + this.name, {
                ignoreLoadingBar: true,
            }).then( angular.bind(this, function(response){
                if(response.data.length > 0)
                    this.results = response.data;
            }));
        } else {
            this.results = [];
        }
     };

     this.set = function(id){
        $http.get('/api/active?id=' + id, {
            ignoreLoadingBar: true,
        }).then( angular.bind(this, function(response){;
            $rootScope.$broadcast('open-marker', response.data);
            leafletData.getMap('civics-map').then(function(map){
                map.setView([response.data.lat, response.data.lng], 18)
            });
            this.results = [];
            this.name    = '';
        }));
     };
});
