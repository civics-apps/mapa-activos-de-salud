angular.module('civics.categories_service', [])

.factory('Categories', function()
{

    this.categories = {
        'pe' : 'Activos de las personas',
        'ae' : 'Activos de las asociaciones y entidades',
        'ap' : 'Activos de la administración pública',
        'fi' : 'Activos físicos',
        'ec' : 'Activos económicos',
        'cu' : 'Activos culturales',
    };

    this.perspectives = {
        'eq' : 'Equidad',
        'pa' : 'Participación',
        'fc' : 'Fortalecimiento comunitario',
        'ot' : 'Otra',
    };

    this.agents = {
        'ip' : 'Instituciones públicas',
        'ci' : 'Ciudadanía',
        'co' : 'Coincidencias',
        'di' : 'Diferencias',
    };

    this.cities = {};

    this.addCity = function(country, name, id, coords){
        if(!this.cities[country])
            this.cities[country] = {}
        if(!this.cities[country][id]) {
            this.cities[country][id] = {
                'name'   : name,
                'coords' : [ coords[1], coords[0] ],
            };
        }
    }

    return this;

})
