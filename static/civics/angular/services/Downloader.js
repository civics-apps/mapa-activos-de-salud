angular.module('civics.downloader', [])

.service('Downloader', function(){

    /**
     *   Download XLS with filtered initiatives
     */
    this.get = function(format, section, selected_categories){

        // Check section and build base URL for the query
        var base_url = "/api/actives_" + format + "?";

        // Build url
        base_url += "&categories=";
        for(var i in selected_categories.categories){
            var topic = selected_categories.categories[i]
            if(topic)
              base_url += topic.toUpperCase() + ",";
        }
        base_url += "&perspectives=";
        for(var i in selected_categories.perspectives){
          var space = selected_categories.perspectives[i];
          if(space)
            base_url += space.toUpperCase() + ",";
        }
        base_url += "&agents=";
        for(var i in selected_categories.agents){
            agent = selected_categories.agents[i];
            if(agent)
              base_url += agent.toUpperCase() + ",";
        }
        base_url += "&cities=";
        for(var i in selected_categories.cities){
            city = selected_categories.cities[i];
            if(city)
              base_url += city + ",";
        }

        // Get items in new window
        window.open(base_url);
  };

  return this;
})
