from django.conf.urls import url, include
from apps.models.models import Active
from . import views

urlpatterns = [
    url(r'^active$', views.active_service, name='get_active'),
    url(r'^actives$', views.actives_service, name='get_actives'),
    url(r'^actives_featured$', views.actives_featured_service, name='get_actives_featured'),
    url(r'^actives_xls$', views.actives_service_xls, name='get_actives_xls'),
    url(r'^actives_csv$', views.actives_service_csv, name='get_actives_csv'),
    url(r'^autocomplete$', views.autocomplete_service, name='autocomplete'),
    url(r'^cities$', views.cities_service, name='get_cities'),
]
