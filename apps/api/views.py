# python
import json, csv, math
from datetime import date, datetime

# django
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from django.shortcuts import render, get_object_or_404
from django.utils.translation import ugettext_lazy as _
from django.http import HttpResponse, JsonResponse
from django.db.models import Count
from django.views.decorators.csrf import csrf_protect
from django.utils.text import slugify
from django.contrib.auth.models import User
from django.db.models import Q
# project
from apps.models.categories import *
from apps.models.models import Active, City
from .serializers import CivicsJSONSerializer

#
#  API
#

no_results = _("No se han encontrado resultados que cumplan con todas las condiciones de filtrado.")

def actives_service(request):
    actives = Active.objects.all()
    return JsonResponse(CivicsJSONSerializer().serialize(actives, fields=(
        'name',
        'position',
        'image',
        'city',
        'category',
        'perspective',
        'perspective_alt',
        'agent',
        'actives'
    )), safe=False)

def actives_service_xls(request):
    import xlwt

    categories   = request.GET.get('categories').split(',')[0:-1];
    perspectives = request.GET.get('perspectives').split(',')[0:-1];
    agents       = request.GET.get('agents').split(',')[0:-1];
    cities       = request.GET.get('cities').split(',')[0:-1];

    actives = Active.objects.all().order_by('city')
    if len(categories) > 0:
        actives = actives.filter(category__in=categories)
    if len(perspectives) > 0:
        actives = actives.filter(perspective__in=perspectives)
    if len(agents) > 0:
        actives = actives.filter(agent__in=agents)
    if len(cities) > 0:
        actives = actives.filter(city__in=cities)

    response = HttpResponse(content_type='application/ms-excel')
    filename = 'activos__' + date.today().isoformat() + '.xls'
    response['Content-Disposition'] = 'attachment; filename=' + filename

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet("Activos")
    ezxf = xlwt.easyxf

    # Write headers
    count_style = ezxf('font: bold on, name monospace, height 320')
    ws.write(0, 0, "Mapa de activos de salud # " + date.today().strftime("%d/%m/%Y"), count_style)
    count_style = ezxf('font: bold off, name monospace, height 240')
    count = str(actives.count()) + " activos encontrados en las categorías: "
    count_categories = ""
    category_keys = dict(CATEGORIES)
    perspective_keys = dict(PERSPECTIVES)
    agent_keys = dict(AGENTS)
    for category in categories:
        if category:
            count_categories += str(categories_keys[category]) + " · "
    for perspective in perspectives:
        if perspective:
            count_categories += str(perspective_keys[perspective]) + " · "
    for agent in agents:
        if agent:
            count_categories += str(agent_keys[agent]) + " · "
    if(count_categories):
        count += " | Filtros: " + count_categories
    ws.write(1, 0, count, count_style)
    ws.row(0).height_mismatch = True
    ws.row(1).height_mismatch = True
    ws.row(0).height = 368
    ws.row(1).height = 368

    row_num = 4
    columns = [
        (u"Nombre del activo", 12000),
        (u"Descripción", 25600),
        (u"Categoría", 12000),
        (u"Perspectiva", 12000),
        (u"Agente", 12000),
        (u"Web", 12000),
        (u"Ciudad", 8000),
        (u"Latitud", 4000),
        (u"Longitud", 4000),
        (u"Fecha de creación", 6000),
    ]
    if request.user.is_staff:
        columns.append( (u"Email", 12000) )

    xlwt.add_palette_colour("header_background", 0x21)
    wb.set_colour_RGB(0x21, 50, 50, 50)
    headers_style = ezxf('font: bold on, color white, name monospace, height 200; align: wrap on, horz center, vert center; borders: bottom thin, top thin; pattern: pattern solid, fore_colour header_background')

    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num][0], headers_style)
        ws.col(col_num).width = columns[col_num][1]
        ws.row(row_num).height_mismatch = True
        ws.row(row_num).height = 368

    firstcol_style = ezxf('font: height 200, name monospace, bold on; align: wrap on, vert center, horiz center')
    cell_style = ezxf('font: height 200, name monospace, bold off; align: wrap on, vert center, horiz center')
    for active in actives:
        row_num += 1
        row = [
            active.name,
            active.description.strip(),
            active.get_category_display(),
            active.get_perspective_display(),
            active.get_agent_display(),
            active.website,
            active.city.name if active.city else "None",
            active.position['coordinates'][0],
            active.position['coordinates'][1],
            active.creation_date.strftime("%d/%m/%Y"),
        ]

        # Active name
        ws.write(row_num, 0, row[0], firstcol_style)
        for col_num in range(1, len(row)):
            content = row[col_num]
            ws.write(row_num, col_num, row[col_num], cell_style)

        content_length = len( active.description )
        characters_per_line = 100
        ws.row(row_num).height_mismatch = True
        ws.row(row_num).height = max(math.ceil(content_length/characters_per_line * 480), 480)

    wb.save(response)
    return response

def actives_service_csv(request):

    query = Q()
    print(request)
    categories = request.GET.get('categories').split(',')[0:-1]
    if len(categories) > 0:
        query = query & Q(category__in=categories)
    perspectives = request.GET.get('perspectives').split(',')[0:-1]
    if len(perspectives) > 0:
        query = query & Q(perspective__in=perspectives)
    agents = request.GET.get('agents').split(',')[0:-1]
    if len(agents) > 0:
        query = query & Q(agent__in=agents)
    cities = request.GET.get('cities').split(',')[0:-1]
    if len(cities) > 0:
        query = query & Q(city__in=cities)

    actives = Active.objects.filter(query)

    response = HttpResponse(content_type='text/csv')
    filename = 'activos__' + date.today().isoformat() + '.csv'
    response['Content-Disposition'] = 'attachment; filename=' + filename

    writer = csv.writer(response)

    writer.writerow(["Mapa de activos de salud # " + date.today().strftime("%d/%m/%Y")])
    category_keys    = dict(CATEGORIES)
    perspective_keys = dict(PERSPECTIVES)
    agent_keys       = dict(AGENTS)
    count = str(actives.count()) + " activos encontrados"
    count_categories = ""
    for category in categories:
        if category:
            count_categories += str(category_keys[category]) + " · "
    for perspective in perspectives:
        if perspective:
            count_categories += str(perspective_keys[perspective]) + " · "
    for agent in agents:
        if agent:
            count_categories += str(agents_keys[agent]) + " · "
    if(count_categories):
        count += " | Filtros: " + count_categories
    writer.writerow([count])

    header = [
        _("Nombre de la iniciativa"),
        _("Descripción"),
        _("Temática"),
        _("Perspectiva"),
        _("Agente"),
        _("Web"),
        _("Ciudad"),
        _("Latitud"),
        _("Longitud"),
        _("Fecha de creación"),
    ]
    writer.writerow(header)

    for active in actives:
        row = [
            active.name,
            active.description.strip(),
            active.get_category_display(),
            active.get_perspective_display(),
            active.get_agent_display(),
            active.website,
            active.city.name if active.city else "None",
            active.position['coordinates'][0],
            active.position['coordinates'][1],
            active.creation_date.strftime("%d/%m/%Y"),
        ]

        writer.writerow(row)

    return response


def autocomplete_service(request):
    """ Returns actives with names beginning with the query characters. """
    type  = request.GET.get('t')
    query = request.GET.get('q')
    if(len(query) < 3):
        raise Exception("Queries must be longer than two characters")

    q = Q(slug__contains=slugify(query))
    if type=='concept':
        q = q|Q(description__contains=query)
    actives = Active.objects.filter(q)
    actives_json = []

    # TODO: map this!
    for active in actives:
        name = query.upper()
        new_name = active.name.upper().replace(name, "<i>" + name + "</i>")
        actives_json.append({
            'id'  : active.pk,
            'nam' : new_name,
        })
    return HttpResponse(json.dumps(actives_json), content_type="application/json")

def media(url):
    uri     = url.split('://')[1]
    service = uri.split('/')[0]
    if service == 'vimeo.com':
        return 'https://player.vimeo.com/video/' + uri.split('/')[1]
    elif service == 'www.youtube.com' and '?v=' in uri:
        return 'https://www.youtube.com/embed/' + uri.split('?v=')[1]
    elif service == 'www.youtube.com' and '&v=' in uri:
        return 'https://www.youtube.com/embed/' + uri.split('&v=')[1]
    elif service == 'youtu.be':
        return 'https://www.youtube.com/embed/' + uri.split('/')[1]
    return ''

def active_service(request):
    id            = request.GET.get('id')
    active        = get_object_or_404(Active, pk=id)
    coords        = active.position['coordinates']
    cityname      = active.city.translated_name(request.LANGUAGE_CODE) if active.city else 'none'
    countryname   = active.city.get_country_display() if active.city else 'none'
    relations     = [ {
        'nam'          : i.name,
        'id'           : i.id,
        'categories'   : i.category.lower(),
        'perspectives' : list( map(lambda i: i.lower(), i.perspective) ),
        'agents'       : list( map(lambda i: i.lower(), i.agent) ),
        'lng'          : i.position['coordinates'][0],
        'lat'          : i.position['coordinates'][1],
    } for i in active.actives.all() ]
    active_json = {
        'id'  : active.pk,
        'nam' : active.name,
        'slu' : active.slug,
        'add' : active.address,
        'cou' : countryname,
        'lng' : coords[0],
        'lat' : coords[1],
        'des' : active.description,
        'img' : active.image.url if active.image else None,
        'web' : active.website,
        'rel' : relations,
        'cities' : cityname,
        'categories' : active.category.lower(),
        'agents' : list( map(lambda i: i.lower(), active.agent) ),
        'perspective' : active.perspective.lower(),
        'perspective_alt' : list( map(lambda i: i.lower(), active.perspective_alt) ),
    }

    return HttpResponse(json.dumps(active_json), content_type="application/json")

def actives_featured_service(request):
    initiatives_json = {}
    initiatives_json['featured'] = []
    initiatives_featured = Active.objects.filter(featured=True).order_by('-creation_date')[:8]
    for initiative in initiatives_featured:
        cityname = initiative.city.translated_name(request.LANGUAGE_CODE) if initiative.city else 'none'
        initiatives_json['featured'].append({
            'nam'    : initiative.name,
            'id'     : initiative.id,
            'img'    : initiative.image_medium.url if initiative.image else None,
            'cities' : cityname,
        })
    initiatives_json['last'] = []
    initiatives_last = Active.objects.order_by('-creation_date')[:8]
    for initiative in initiatives_last:
        cityname = initiative.city.translated_name(request.LANGUAGE_CODE) if initiative.city else 'none'
        initiatives_json['last'].append({
            'nam'    : initiative.name,
            'id'     : initiative.id,
            'img'    : initiative.image_medium.url if initiative.image else None,
            'cities' : cityname,
        })

    return HttpResponse(
        json.dumps(initiatives_json),
        content_type="application/json"
    )


def cities_service(request):
    """
    Get cities related to initiatives
    """

    cities = City.objects.all()
    current_lang = request.LANGUAGE_CODE
    response = {}
    for city in cities:
        response[city.pk] = {
            'name'        : city.translated_name(current_lang),
            'id'          : city.pk,
            'country'     : city.country.name,
            'coordinates' : city.position['coordinates']
        }
    return HttpResponse(
        json.dumps(response),
        content_type="application/json"
    );
