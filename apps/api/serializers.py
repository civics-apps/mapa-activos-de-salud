# django
from django.core.serializers.json import Serializer

class CivicsJSONSerializer(Serializer):
    def get_dump_object(self, obj):
        data = self._current
        data['image']           = obj.image_medium.url if obj.image_medium else None
        data['pk']              = obj.pk
        data['category']        = obj.category.lower()
        data['agent']           = list( map(lambda i: i.lower(), obj.agent) )
        if 'ci' in data['agent'] and 'ip' in data['agent']:
            data['agent'].append('co')
        else:
            data['agent'].append('di')
        data['perspective']     = obj.perspective.lower()
        data['perspective_alt'] =  list( map(lambda i: i.lower(), obj.perspective_alt) )
        return data
