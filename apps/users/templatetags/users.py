# django
from django import template
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
# project
from apps.models.models import Active
register = template.Library()


@register.inclusion_tag('dashboard_leaflet.js.html')
def dashboard_leaflet_js(user_actives=None):
    """ Tag to embed scripts needed to display a map with user's actives in dashboard """
    initiatives = user_actives
    return locals()
