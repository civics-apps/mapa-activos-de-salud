# django
from django.contrib import admin

# contrib
from leaflet.admin import LeafletGeoAdmin
from imagekit import ImageSpec
from imagekit.admin import AdminThumbnail
from imagekit.processors import ResizeToFill
from imagekit.cachefiles import ImageCacheFile

# project
from . import models

# Custom actions
def feature(modeladmin, request, queryset):
    queryset.update(featured=True)

feature.short_description = "Destaca los proyectos seleccionados"

def unfeature(modeladmin, request, queryset):
    queryset.update(featured=False)

unfeature.short_description = "Dejar de destacar de los proyectos seleccionados"

# Active Admin Form
class ActiveAdmin(LeafletGeoAdmin):
    model = models.Active
    ordering = (
        'name',
    )
    list_display = (
        'name',
        # 'creation_date',
        'perspective',
        'category',
        'agent',
        'featured'
    )
    list_filter = (
        'featured',
        'perspective',
        'category',
        'agent',
        'city'
    )
    actions = [unfeature, feature]

    def get_action_choices(self, request):

        return super(ActiveAdmin, self).get_action_choices(request, [("", "Elige una acción")])

# City Admin Form
class CityAdmin(LeafletGeoAdmin):
    model = models.City
    ordering = ('name',)
    list_display = (
        'city_name',
        'country',
        'related_actives'
    )
    list_filter  = ('country',)

    def city_name(self, obj):
        return "%s [%s]" % (obj.name, obj.id)

    def related_actives(self, obj):
        return models.Active.objects.filter(
            city=obj
        ).count()

    def get_action_choices(self, request):

        return super(CityAdmin, self).get_action_choices(
            request,
            [("", "Elige una acción")]
        )


# Register models in admin
admin.site.register(models.City, CityAdmin)
admin.site.register(models.Active, ActiveAdmin)
admin.site.register(models.Resource)
