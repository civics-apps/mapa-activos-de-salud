from django.core.management.base import BaseCommand, CommandError
import csv, json, os.path, random
from apps.models.models import Active, City
from django.utils.text import slugify

# TODO: tests!!!

"""
A manage.py command to migrate initiatives from a CSV file
"""

class Command(BaseCommand):
    help = "Instantiate Actives models from a CSV file. Following columns are needed: \
            'Name', 'Code' (contrycode in ISO ISO 3166-2), 'Longitude' and 'Latitude' \
            The only argument is a valid path to the CSV file."

    """
    Add CSV file as an argument to the parser
    """
    def add_arguments(self, parser):
        parser.add_argument('csv_file')

    """
    Imports Active models from a given CSV file
    """
    def handle(self, *args, **options):
        if not os.path.isfile(options['csv_file']):
             raise CommandError('The specified file does not exist. Have you written it properly?')
        with open(options['csv_file'], 'r') as actives_file:
            actives = csv.DictReader(actives_file)
            actives_objects = []
            for i in actives:
                print('Creando activo ' + i['nombre'].strip())
                city = City.objects.filter(name=i['municipio'].strip()).first()
                address = i['dirección']
                perspectives = i['perspectiva'].split(',')
                coords = city.position['coordinates']
                lat = str(coords[0] + (random.random()-.5) * .001)
                lon = str(coords[1] + (random.random()-.5) * .001)
                actives_objects.append(
                    Active(
                        name        = i['nombre'].strip(),
                        slug        = slugify(i['nombre']),
                        category    = i['categoría'],
                        perspective = perspectives[0],
                        perspective_alt = perspectives[1:] if len(perspectives) > 1 else [],
                        agent       = i['agente'],
                        description = i['descripción'],
                        city        = city,
                        address     = i['dirección'],
                        position    = json.loads('{ "type": "Point", "coordinates": [' + lat + ',' + lon +'] }')
                    )
                )
            Active.objects.bulk_create(actives_objects)
