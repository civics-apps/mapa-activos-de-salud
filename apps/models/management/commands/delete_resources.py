from django.core.management.base import BaseCommand, CommandError
from apps.models.models import Resource

# TODO: tests!!!

"""
A manage.py command to migrate cities from a CSV file
"""

class Command(BaseCommand):
    help = "Delete all Active models from database."

    """
    Deletes all Initiative objects
    """
    def handle(self, *args, **options):
        Resource.objects.all().delete()
