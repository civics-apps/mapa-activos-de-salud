# django
from django.utils.translation import ugettext_lazy as _
from django import forms
from django_countries import countries
from django.core.exceptions import ValidationError
from django.contrib.admin.widgets import FilteredSelectMultiple
from django.urls import reverse_lazy
from django.utils.text import slugify
from django.contrib.auth.models import User
# custom
from django.conf import settings
from . import models, categories
from apps.utils import widgets
from apps.utils import fields

def group_label(country_key):
    if country_key:
        return dict(countries)[country_key]
    return None

class CityForm(forms.ModelForm):
    """Generic modelform to create and update City objects"""

    class Meta:
        model = models.City
        fields = '__all__'
        widgets = {
            'position' : widgets.GeocodedLeafletWidget(
                submit_text=_('Localiza la dirección'),
                provider="nominatim"
            )
        }

    def clean_name(self):
        name   = self.cleaned_data['name']
        cities = models.City.objects.all()
        slugs = [ slugify(city.name) for city in cities ]
        if slugify(name) in slugs:
             raise forms.ValidationError(_('Ya existe una ciudad con ese nombre'))
        return name

class RelationsForm(forms.ModelForm):
    """Modelform to set relationships among actives"""

    actives = forms.ModelMultipleChoiceField(
        queryset = models.Active.objects.order_by('name'),
        label=_('Activos relacionados'),
        help_text=_(
            'Escoge de la columna izquierda los activos relacionados '
            'y selecciónalos pasándolos a la columna derecha pulsando el botón "Elegir". '
            'Puedes usar el filtro de la columna izquierda para buscar los activos '
            'por su nombre. Ten en cuenta que éste es sensible a mayúsculas, '
            'minúsculas y signos de puntuación. Puedes hacer selecciones múltiples '
            'con la tecla Ctrl pulsada (Command en MAC)'
        ),
        required=False,
        widget=FilteredSelectMultiple(
            _('Elementos'),
            False,
        )
    )

    def __init__(self, *args, **kwargs):
        super(RelationsForm, self).__init__(*args, **kwargs)
        self.fields['actives'].queryset = models.Active.objects.all().order_by('name')

    class Meta:
        model = models.Active
        fields = ['actives',]

    class Media:
        js = [
            reverse_lazy('javascript-catalog'),
        ]

    def clean_actives(self):
        cleaned_actives = self.cleaned_data.get('actives')
        if cleaned_actives.filter(pk=self.instance.pk).exists():
            raise forms.ValidationError(_(
                'No puedes añadir tu iniciativa a este campo. Seleccionala en '
                'la columna derecha y usa el botón "Eliminar" para quitarla de '
                'la selección.'
            ))
        return cleaned_actives


class CityField(fields.GroupedModelChoiceField):
    """City field"""

    def label_from_instance(self, obj):
        return obj.name

class ActiveForm(forms.ModelForm):
    """Generic modelform to create and update Active objects"""

    city = CityField(
        queryset=models.City.objects.order_by(
            'country',
            'name'
        ),
        label=_('Ciudad'),
        help_text=_(
            'Ciudad donde se encuentra el activo. Si no encuentras la ciudad '
            'en el desplegable usa el botón inferior para añadirla.'
        ),
        group_by_field='country',
        group_label=group_label,
        empty_label=" ",
        widget = widgets.SelectOrAddWidget(
            view_name='modelforms:create_city_popup',
            link_text=_("Añade una ciudad")
        )
    )

    class Meta:
        model=models.Active
        exclude=[ 'actives', ]
        widgets={
            'image' : widgets.PictureWithPreviewWidget(),
            'description' : widgets.LimitedTextareaWidget(
                limit=500
            ),
            'position' : widgets.GeocodedLeafletWidget(
                submit_text=_('Localiza la dirección'),
                provider="nominatim"
            )
        }

    def __init__(self, *args, **kwargs):
        self.base_fields['user'].queryset = User.objects.all().order_by('username')
        super(ActiveForm, self).__init__(*args, **kwargs)
