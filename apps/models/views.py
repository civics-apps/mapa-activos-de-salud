# django
from django.utils.translation import ugettext_lazy as _
from django.shortcuts import render
from django.urls import reverse_lazy, reverse
from django.http import HttpResponseRedirect, Http404
from django.core.exceptions import PermissionDenied
from django.contrib.auth.mixins import UserPassesTestMixin
from django.shortcuts import get_object_or_404
from django.views.generic.list import ListView
# project
from apps.utils.views import GenericCreate, GenericUpdate, GenericDelete
from . import forms, models
from .models import Active
from .categories import RESOURCES

# Model related templates

modelform_generic_template = 'pages/modelform.html'
modelform_delete_template  = 'pages/modelform--delete.html'
modelform_relations_template  = 'pages/modelform--relations.html'

#
#  Active
#

class ActiveCreate(GenericCreate):
  """Generic view to create Active objects."""

  model            = models.Active
  form_class       = forms.ActiveForm
  form__html_class = 'active'
  template_name    = modelform_generic_template
  title            = _('Crea un activo [1/2]')

  def form_valid(self, form):
    form.instance.user = self.request.user
    active = form.save()
    return HttpResponseRedirect(
        reverse_lazy('modelforms:relate_active', kwargs={'pk' : active.pk })
    )

  def get_context_data(self, **kwargs):
    """Pass context data to generic view."""
    context = super(ActiveCreate, self).get_context_data(**kwargs)
    context['form__html_class'] = self.form__html_class
    context['form__action_class'] = 'form-create'
    context['submit_text'] = _('Crea el activo y añade relaciones con otros activos')
    return context


class ActiveEdit(GenericUpdate):
  """Generic view to edit Active objects."""
  model            = models.Active
  form_class       = forms.ActiveForm
  form__html_class = 'active'
  template_name    = modelform_generic_template
  title            = _('Edita la información del activo')
  success_url      = reverse_lazy('front')

  def get_success_url(self):
    return reverse_lazy('front')

  def form_valid(self, form):
    form.instance.user = self.request.user
    return super(ActiveEdit, self).form_valid(form)

  def get_context_data(self, **kwargs):
    """Pass context data to generic view."""
    context = super(ActiveEdit, self).get_context_data(**kwargs)
    pk = self.kwargs['pk']
    try:
        active = models.Active.objects.get(pk=pk)
    except:
        raise Http404("That active does not exist")
    context['title'] = self.title + (' ') + active.name
    context['form__html_class'] = self.form__html_class
    context['form__action_class'] = 'form-edit'
    context['object_id'] = pk
    context['submit_text'] = _('Guarda los cambios')
    return context

class ActiveRelate(GenericUpdate):
  """Generic view to edit Active objects."""
  model = models.Active
  form_class = forms.RelationsForm
  form__html_class = 'relations'
  template_name = modelform_generic_template
  title = _('Relaciona tu activo')
  success_url = reverse_lazy('front')

  def get_success_url(self):
    return reverse_lazy('front')

  def get_context_data(self, **kwargs):
    """Pass context data to generic view."""
    context = super(ActiveRelate, self).get_context_data(**kwargs)
    pk = self.kwargs['pk']
    active = get_object_or_404(models.Active, pk=pk)
    context['title'] = self.title
    context['form__html_class'] = self.form__html_class
    context['form__action_class'] = 'form-relate'
    context['object_id'] = pk
    context['submit_text'] = _('Guarda los cambios')
    return context


class ActiveDelete(GenericDelete):
  """Generic view to delete Active objects."""
  model = models.Active
  form_class = forms.ActiveForm
  form__html_class = 'active'
  template_name = modelform_delete_template
  title = _('Borra la iniciativa ')
  success_url = reverse_lazy('front')

  def get_context_data(self, **kwargs):
    """Pass context data to generic view."""
    context = super(ActiveDelete, self).get_context_data(**kwargs)
    pk = self.kwargs['pk']
    active = get_object_or_404(models.Active, pk=pk)
    context['title'] = self.title + (' ') + active.name
    context['form__html_class'] = self.form__html_class
    context['submit_text'] = _('¿Estás seguro de que quieres borrar este activo?')
    context['cancel_text'] = _('No, no estoy seguro.')
    return context

  def get_success_url(self):
    return reverse_lazy('front')


class Resources(ListView):

    model = models.Resource
