# django
from django.conf.urls import url
from django.views.generic import TemplateView
# project
from apps.utils.views import PopupFormView
from . import views
from .forms import CityForm

urlpatterns = [
    url(
        r'^crea/ciudad$',
        PopupFormView.as_view(
            form_class=CityForm,
            template_name="forms/city-popup-form.html"
        ),
        name="create_city_popup"
    ),
    url(
        r'^crea/activo$',
        views.ActiveCreate.as_view(),
        name="create_active"
    ),
    url(
        r'^edita/activo/(?P<pk>.+)$',
        views.ActiveEdit.as_view(),
        name="edit_active"
    ),
    url(
        r'^relaciona/activo/(?P<pk>.+)$',
        views.ActiveRelate.as_view(),
        name="relate_active"
    ),
    url(
        r'^borra/activo/(?P<pk>.+)$',
        views.ActiveDelete.as_view(),
        name="delete_active"
    ),
    url(
        r'^bienvenido$',
        TemplateView.as_view(
            template_name='pages/initiative-success.html'
        ),
        name="welcome_initiative"
    ),
]
