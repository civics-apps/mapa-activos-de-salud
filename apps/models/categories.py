from django.utils.translation import ugettext_lazy as _

"""
The different categories used by the Models of Civics
"""

CATEGORIES = (
    ('PE', _('Activos de las personas')),
    ('AE', _('Activos de las asociaciones y entidades')),
    ('AP', _('Activos de la administración pública')),
    ('FI', _('Activos físicos')),
    ('EC', _('Activos económicos')),
    ('CU', _('Activos culturales')),
)

PERSPECTIVES = (
    ('EQ', _('Equidad')),
    ('PA', _('Participación')),
    ('FC', _('Fortalecimiento comunitario')),
    ('OT', _('Otra'))
)

AGENTS = (
    ('IP', _('Instituciones públicas')),
    ('CI', _('Ciudadanía')),
)

RESOURCES = ()
