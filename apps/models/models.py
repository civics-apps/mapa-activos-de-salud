# django
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User
from django.utils import timezone
from django.utils.text import slugify
from django.core.mail import send_mail
from django.conf import settings
# contrib
from imagekit.models import ImageSpecField
from imagekit import processors
from leaflet.forms.widgets import LeafletWidget
from django_countries.fields import CountryField
from djgeojson.fields import PointField
from multiselectfield import MultiSelectField
# project
from . import categories
from .validators import ImageTypeValidator, ImageSizeValidator, AttachedFileValidator
from .utils import RenameCivicsImage


# Bound methods moved from model to avoid problems with serialization in migrations

validate_image_size = ImageSizeValidator({
    'min_width'  : 600,
    'min_height' : 300,
    'max_width'  : 1920,
    'max_height' : 1280
})
validate_image_type = ImageTypeValidator(["jpeg", "png"])
validate_file_type  = AttachedFileValidator()
actives_images_path = RenameCivicsImage("images/actives/")

#
#  City
#

class City(models.Model):
  """Model to represent City objects"""

  name = models.CharField(
    _('Nombre de la ciudad [EU]'),
    max_length = 200,
    blank = False,
    null = True,
    help_text = _(
        'Especifica el nombre de la ciudad en euskera'
    )
  )
  name_es = models.CharField(
    _('Nombre de la ciudad [ES]'),
    max_length = 200,
    blank = False,
    null = True,
    help_text = _(
        'Especifica el nombre español de la ciudad'
    )
  )
  country = CountryField(_('País'),
    null=True,
    help_text = _('¿A qué país pertenece la ciudad?'),
    default = 'ES',
    blank=True,
  )
  position = PointField(
    _("Ubicación"),
    blank=False,
    null=True,
    help_text=_("Añade la ubicación de la ciudad.")
  )

  def translated_name(self, langcode):
      namefield = 'name_' + langcode
      if langcode != 'eu' and getattr(self, namefield):
          return getattr(self, namefield)
      return self.name

  class Meta:
    ordering = ('name',)
    verbose_name = _('Ciudad')
    verbose_name_plural = _('Ciudades')

  def __str__(self):
    """String representation of this model objects."""
    return "%s [%s]" % (self.name, self.id) or '---'


#
#  Initiative
#

class Active(models.Model):
  """Model to represent Active objects"""

  name = models.CharField(
    _('Nombre del activo'),
    max_length=200,
    blank=False,
    null=True,
    help_text=_('Especifica el nombre del activo')
  )
  slug = models.SlugField(
    editable=False,
    blank=True
  )
  creation_date = models.DateField(
    editable=False,
    default=timezone.now
  )
  featured = models.BooleanField(
    _('Destacado'),
    blank=True,
    default=False,
    help_text=_('Indica si es un activo destacado')
  )
  user = models.ForeignKey(
    User,
    verbose_name=_('Gestora o gestor'),
    blank=True,
    null=True,
    on_delete=models.SET_NULL
  )
  image = models.ImageField(
    _("Imagen"),
    blank=True,
    validators=[
        validate_image_size,
        validate_image_type
    ],
    upload_to=actives_images_path,
    help_text=_(
        "Sube una imagen representativa del activo "
        "haciendo click en la imagen inferior. "
        "La imagen ha de tener ancho mínimo de 600 píxeles "
        "y máximo de 1920, y altura mínima de 300 píxeles "
        "y máxima de 1280. Formatos permitidos: PNG, JPG, JPEG."
    )
  )
  image_medium = ImageSpecField(
    source="image",
    processors=[
        processors.ResizeToFill(600, 300)
    ],
    format='JPEG',
    options={'quality': 90}
  )
  description = models.TextField(
    _('Descripción del activo'),
    blank=False,
    null=True,
    help_text=_(
        'Describe el activo. ¿Qué crees que aporta este activo a tu bienestar '
        'y\/o al bienestar de la comunidad?'
    )
  )
  website = models.URLField(
    _('Website'),
    blank=True,
    null=True,
    help_text=_('Puedes enlazar una web que ayude a conocer mejor el activo.')
  )
  perspective = models.CharField(
    _('Perspectiva principal'),
    blank=False,
    null=False,
    max_length=2,
    choices = categories.PERSPECTIVES,
    help_text=_('Perspectiva principal del activo. Definirá el icono del activo')
  )
  perspective_alt = MultiSelectField(
    _('Otras perspectivas'),
    blank=True,
    max_length=10,
    choices = categories.PERSPECTIVES,
    help_text=_('Otras perspectivas del activo. No hace falta que repitas la principal')
  )
  category = models.CharField(
    _('Categoría del activo'),
    blank=False,
    null=False,
    max_length=2,
    choices = categories.CATEGORIES,
    help_text=_('Categoría del activo')
  )
  agent = MultiSelectField(
    _('Agente'),
    blank=False,
    null=False,
    max_length=10,
    choices = categories.AGENTS,
    help_text=_('Agente que mapea el activo')
  )
  city = models.ForeignKey(
    City,
    verbose_name=_('Ciudad'),
    blank=False,
    null=True,
    on_delete=models.SET_NULL,
    help_text=_(
        'Ciudad donde se encuentra el activo. Si no encuentras la ciudad '
        'en el desplegable usa el botón inferior para añadirla.'
    )
  )
  address = models.CharField(
    _('Dirección'),
    max_length=200,
    blank=False,
    null=True,
    help_text=_(
        'Dirección del activo. No es necesario que vuelvas a '
        'introducir la ciudad del activo.'
    )
  )
  position = PointField(
    _("Ubicación"),
    blank=False,
    null=True,
    help_text=_(
        "Tras añadir ciudad y dirección puedes ubicar el activo pulsando "
        "el botón inferior y ajustando la posición del marcador posteriormente."
    )
  )
  # Relations
  actives = models.ManyToManyField(
    'self',
    verbose_name=_('Activos relacionados'),
    blank=True
  )

  class Meta:
    verbose_name        = _('Activo')
    verbose_name_plural = _('Activos')
    ordering = ['city__name', 'name']

  def __str__(self):
    """String representation of this model objects."""
    return self.name or '---'

  def save(self, *args, **kwargs):
    """Populates automatically 'slug' field and notifies by email"""

    if not self.id:
        self.slug = slugify(self.name)

    super(Active, self).save(*args, **kwargs)

  def edit_permissions(self, user):
    """Returns users allowed to edit an instance of this model."""
    if user.is_staff or (self.user and self.user == user):
      return True
    return False


class Resource(models.Model):

    name = models.CharField(
        _('Nombre del recurso'),
        max_length = 200,
        blank=False,
        null=True,
    )
    name_es = models.CharField(
        _('Nombre del recurso en español'),
        max_length = 200,
        blank=False,
        null=True,
    )
    image = models.ImageField(
        _("Imagen"),
        blank=True,
        validators=[validate_image_size, validate_image_type],
        upload_to='images/resources',
    )
    file = models.FileField(
        _("Archivo"),
        blank=True,
        upload_to='files/resources',
    )
    url = models.URLField(
        _("Enlace"),
        blank=True,
        help_text=_('Si el recurso es un site externo puedes indicar la url aquí.')
    )
    summary = models.TextField(
        _('Descripción corta del recurso'),
        blank=False,
        null=True,
    )
    summary_es = models.TextField(
        _('Descripción corta del recurso [español]'),
        blank=False,
        null=True,
    )

    class Meta:
      verbose_name        = _('Recurso')
      verbose_name_plural = _('Recursos')

    def __str__(self):
        """String representation of this model objects."""
        return self.name

    def get_name(self, lang):
        return self.name if lang==settings.LANGUAGE_CODE else getattr(self, 'name_'+lang)

    def get_summary(self, lang):
        return self.summary if lang==settings.LANGUAGE_CODE else getattr(self, 'summary_'+lang)
